# weatherApp

Il s'agit de mon projet météo.

## Comment lancer l'application

Le code source est disponible sur GitLab. Pour utiliser l'application, il suffit de lancer la commande "git pull" pour récupérer les fichiers, puis de lancer l'application.

## Fonctionnalités

L'application permet d'obtenir la météo grâce à la géolocalisation ou en entrant le nom d'une ville. Les prévisions sont disponibles par heure et pour une période de 3 jours seulement, car l'API choisie ne permet pas de récupérer des prévisions sur une période plus longue.

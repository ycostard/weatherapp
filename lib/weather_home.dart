// ignore_for_file: await_only_futures
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:weatherapp/city_models.dart';
import 'package:weatherapp/weather_models.dart';
import 'package:weatherapp/weather_services.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class WeatherHome extends StatefulWidget {
  const WeatherHome({super.key});

  @override
  _WeatherHomeState createState() => _WeatherHomeState();
}

class _WeatherHomeState extends State<WeatherHome> {
  String cityName = "Rennes";
  late WeatherModels weather;
  List<String> data = [];

  @override
  void initState() {
    super.initState();
    getWeatherGPS();
  }

  getWeatherGPS() async {
    // Récupérer la position de l'utilisateur
    //WeatherServices().getWeatherDataWithGeolocator();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    // Utiliser les coordonnées de la position pour obtenir la météo
    double lat = position.latitude;
    double lon = position.longitude;

    setState(() {
      cityName = '$lat,$lon';
    });
  }

  List<String> _data = [];

  Future<void> _findCity(String query) async {
    if (query.length >= 2) {
      String? apiUrl = dotenv.env['API_CITY_URL'];
      final url = '$apiUrl?name=$query';
      final response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        try {
          var jsonResponse = jsonDecode(response.body);
          var cityModels = CityModels.fromJson(jsonResponse);
          List<String> results = cityModels.cities;

          setState(() {
            _data = results;
          });
        } catch (err) {
          setState(() {
            _data = [];
          });
        }
      } else {
        throw Exception('Failed to search cities');
      }
    } else {
      setState(() {
        _data = [];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Autocomplete<String>(optionsBuilder: (textEditingValue) async {
          await _findCity(textEditingValue.text);
          return _data.where(
            (entry) => entry.contains(
              textEditingValue.text,
            ),
          );
        }, onSelected: (value) async {
          setState(() {
            cityName = value;
          });
        }),
        actions: [
          IconButton(
            icon: const Icon(Icons.my_location),
            onPressed: () {
              getWeatherGPS();
            },
          ),
        ],
        backgroundColor: const Color.fromARGB(255, 108, 218, 248),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),
        ),
        titleSpacing: 16,
      ),
      drawer: const Drawer(
        child: Text(
            "Historique mais je n'ai pas réussi à le mettre en place avec SharedPreferences"),
      ),
      body: FutureBuilder<WeatherModels>(
        future: WeatherServices().getWeatherData(
            cityName), // Remplacez cette fonction par votre propre fonction qui retourne un Future<Weather>
        builder: (BuildContext context, AsyncSnapshot<WeatherModels> snapshot) {
          if (snapshot.hasData) {
            final weather = snapshot.data!;
            return Column(
              children: [
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
                    child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(weather.ville,
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 30,
                                  fontWeight: FontWeight.w600)),
                          Text(weather.date,
                              style: const TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                              )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/info.png',
                                width: 20,
                                height: 20,
                              ),
                              Text(
                                utf8.decode(weather.description.runes.toList()),
                                style: const TextStyle(
                                  fontSize: 20,
                                  color: Color.fromARGB(255, 32, 197, 243),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Style de texte pour les degrés
                        Text(
                          '${weather.temperature}°C',
                          style: const TextStyle(
                            fontSize: 50,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(width: 20),
                        // Image du temps
                        Image.network(
                          weather.icon,
                          width: 100,
                          height: 100,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(
                            10), // ajout des coins arrondis
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 4,
                            spreadRadius: 1,
                            offset: const Offset(0, 1),
                          )
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16,
                            vertical:
                                8), // utilisation de symmetric pour avoir un padding égal à gauche et à droite
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(left: 8.0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Prévisions heure par heure",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 8),
                            Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    Colors.blue.shade700.withOpacity(0.4),
                                    Colors.transparent,
                                  ],
                                ),
                                borderRadius: BorderRadius.circular(
                                    10), // ajout des coins arrondis
                              ),
                              child: SizedBox(
                                height: 100,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: [
                                    for (var i = 0;
                                        i < weather.prevision.length;
                                        i++)
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16,
                                            vertical:
                                                8), // utilisation de symmetric pour avoir un padding égal à gauche et à droite
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                                "${DateTime.parse(weather.prevision[i]['time']).hour}h",
                                                style: const TextStyle(
                                                    color: Colors.white)),
                                            Image.network(
                                              weather.prevision[i]['condition']
                                                  ['icon'],
                                              width: 30,
                                              height: 30,
                                            ),
                                            Text(
                                                '${weather.prevision[i]['temp_c']}°C',
                                                style: const TextStyle(
                                                    color: Colors.white)),
                                          ],
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 6, 111, 197),
                        borderRadius: BorderRadius.circular(
                            10), // ajout des coins arrondis
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 4,
                            spreadRadius: 1,
                            offset: const Offset(0, 1),
                          )
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16,
                            vertical:
                                8), // utilisation de symmetric pour avoir un padding égal à gauche et à droite
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(left: 8.0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Prévisions sur 3 jours",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 8),
                            Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    Colors.blue.shade700.withOpacity(0.4),
                                    Colors.transparent,
                                  ],
                                ),
                                borderRadius: BorderRadius.circular(
                                    10), // ajout des coins arrondis
                              ),
                              child: SizedBox(
                                height: 100,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: [
                                    for (var i = 0;
                                        i < weather.previsionJours.length;
                                        i++)
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16,
                                            vertical:
                                                8), // utilisation de symmetric pour avoir un padding égal à gauche et à droite
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              DateFormat('EEEE', 'fr_FR')
                                                  .format(DateTime.parse(
                                                      weather.previsionJours[i]
                                                          ['date'])),
                                              style:
                                                  const TextStyle(fontSize: 16, color: Colors.white),
                                            ),
                                            Image.network(
                                              weather.previsionJours[i]['day']
                                                  ['condition']['icon'],
                                              width: 30,
                                              height: 30,
                                            ),
                                            Text(
                                              '${weather.previsionJours[i]['day']['avgtemp_c']}°C',
                                              style:
                                                  const TextStyle(fontSize: 16, color: Colors.white),
                                            ),
                                          ],
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return Text('Erreur: ${snapshot.error}');
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}

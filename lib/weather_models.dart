import 'dart:convert';

class WeatherModels {
  String ville;
  String region;
  String pays;
  double temperature;
  String description;
  String icon;
  String date;
  int humidite;
  double vent;
  List prevision;
  List previsionJours;

  WeatherModels(
      {this.ville = '',
      this.region = '',
      this.pays = '',
      this.temperature = 0,
      this.description = '',
      this.icon = '',
      this.date = '',
      this.humidite = 0,
      this.vent = 0,
      this.prevision = const [],
      this.previsionJours = const []});
  // Factory method qui prend en entrée un objet json et renvoie une instance de WeatherModels
  factory WeatherModels.weatherJson(json) {
    return WeatherModels(
      ville: json['location']['name'], // On récupère la ville depuis l'objet json
      region: json['location']['region'], // On récupère la région depuis l'objet json
      pays: json['location']['country'], // On récupère le pays depuis l'objet json
      temperature: json['current']['temp_c'], // On récupère la température actuelle en °C depuis l'objet json
      description: json['current']['condition']['text'], // On récupère la description de la condition météorologique actuelle depuis l'objet json
      icon: json['current']['condition']['icon'], // On récupère l'icône de la condition météorologique actuelle depuis l'objet json
      date: json['location']['localtime'], // On récupère la date et l'heure actuelles depuis l'objet json
      humidite: json['current']['humidity'], // On récupère l'humidité actuelle en pourcentage depuis l'objet json
      vent: json['current']['wind_kph'], // On récupère la vitesse du vent actuelle en km/h depuis l'objet json
      prevision: json['forecast']['forecastday'][0]['hour'], // On récupère les prévisions horaires pour la journée en cours depuis l'objet json
      previsionJours: json['forecast']['forecastday'] // On récupère les prévisions pour les jours suivants depuis l'objet json
    );
  }
}

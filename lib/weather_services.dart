import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:weatherapp/weather_models.dart';
import 'package:http/http.dart' as http;

class WeatherServices{

  Future<WeatherModels> getWeatherData(name) async {

    String? apiMeteoUrl = dotenv.env['API_METEO_URL'];
    // création de l'URL de l'API en fonction du nom de la ville fourni
    var uri = Uri.parse("$apiMeteoUrl&q=$name");

    // appel de l'API en utilisant la librairie 'http'
    final response = await http.get(uri);

    // vérification que la réponse de l'API est valide (code 200)
    if(response.statusCode == 200){
      // conversion de la réponse JSON en objet 'WeatherModels' grâce à la méthode 'weatherJson' de la classe 'WeatherModels'
      return WeatherModels.weatherJson(jsonDecode(response.body));
    } else {
      // si la réponse n'est pas valide, on lance une erreur
      return throw 'Erreur';
    }
  }
}
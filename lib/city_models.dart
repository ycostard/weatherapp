class CityModels {
  List<String> cities;

  CityModels({this.cities = const [], required String city});

  factory CityModels.fromJson(Map<String, dynamic> json) {
    List<String> cityNames = [];
    List<dynamic> results = json['results'];
    for (var result in results) {
      String cityName = result['name'];
      cityNames.add(cityName);
    }
    return CityModels(cities: cityNames, city: '');
  }
}